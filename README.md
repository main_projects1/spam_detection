# Spam Detection project 

Проект по обнаружению спама. В качестве датасета для обучения алгоритма взят Enron Spam Dataset: https://www.kaggle.com/datasets/wanderfj/enron-spam/code.
Тестирование классификации производилось с использованием предоставленного набора. 
Основные этапы препроцессинга данных и обучения представлены в jupyter notebook. 

